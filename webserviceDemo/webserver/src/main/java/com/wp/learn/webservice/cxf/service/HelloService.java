/*
 * Copyright (C) 2017 Baidu, Inc. All Rights Reserved.
 */
package com.wp.learn.webservice.cxf.service;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by v_wangping02 on 2017/11/29.
 */
@WebService
public interface HelloService {
    String sayHi(String name);
}
