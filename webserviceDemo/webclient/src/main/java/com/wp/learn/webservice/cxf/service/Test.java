/*
 * Copyright (C) 2017 Baidu, Inc. All Rights Reserved.
 */
package com.wp.learn.webservice.cxf.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by v_wangping02 on 2017/12/1.
 */
public class Test {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-ws.xml");
        IHelloService helloService = (IHelloService) context.getBean("helloService");
        String result = helloService.sayHi("帅哥");
        System.out.print(result);
    }
}
