package com.wp.learn.webservice.cxf.service;

import javax.jws.WebService;

/**
 * Created by v_wangping02 on 2017/12/1.
 * 这里的接口和方法名自己随便写
 */

//注意，该出的targetNamespace的值必须和webService服务项目中定义的必须一致，否则调用不成功
@WebService(targetNamespace = "http://impl.service.cxf.webservice.learn.wp.com/", name = "IHelloService")
public interface IHelloService {
    //接口名称可以不一样，方法名称、参数格式必须保持一致，否则无法找到服务的实现的方法
    public String sayHi(String name);
}

